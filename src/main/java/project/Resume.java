package project;

import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class Resume {
    private static final String FILE = "results/Resume.pdf";

    private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 15,
            Font.BOLD);
    private static Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.BOLD);
    private static Font regularFont = new Font(Font.FontFamily.TIMES_ROMAN, 10,
            Font.NORMAL);


    private String myFirstName;
    private String myLastName;
    private String myProfessionName;
    private String myEducation;
    private String mySummary;


    public Resume(String myFirstName, String myLastName, String myProfessionName, String myEducation, String mySummary) {
        this.myFirstName = myFirstName;
        this.myLastName = myLastName;
        this.myProfessionName = myProfessionName;
        this.myEducation = myEducation;
        this.mySummary = mySummary;
    }


    public void createPdf() {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addTitlePage(document);
            createTable(document, myFirstName, myLastName, myEducation, myProfessionName, mySummary);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("MY RESUME", titleFont));
        preface.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(preface, 2);
        document.add(preface);
    }


    private void createTable(Document document, String name, String lastName, String education, String profession, String summary)
            throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.addCell(createCell("First Name", boldFont));
        table.addCell(createCell(name, regularFont));

        table.addCell(createCell("Last Name", boldFont));
        table.addCell(createCell(lastName, regularFont));

        table.addCell(createCell("Profession", boldFont));
        table.addCell(createCell(profession, regularFont));

        table.addCell(createCell("Education", boldFont));
        table.addCell(createCell(education, regularFont));

        table.addCell(createCell("Summary", boldFont));
        table.addCell(createCell(summary, regularFont));

        document.add(table);

    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private PdfPCell createCell(String name, Font font) {
        PdfPCell cell = new PdfPCell(new Phrase(name, font));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPadding(10);
        return cell;
    }

    public String getMyFirstName() {
        return myFirstName;
    }

    public void setMyFirstName(String myFirstName) {
        this.myFirstName = myFirstName;
    }

    public String getMyLastName() {
        return myLastName;
    }

    public void setMyLastName(String myLastName) {
        this.myLastName = myLastName;
    }

    public String getMyProfessionName() {
        return myProfessionName;
    }

    public void setMyProfessionName(String myProfessionName) {
        this.myProfessionName = myProfessionName;
    }

    public String getMyEducation() {
        return myEducation;
    }

    public void setMyEducation(String myEducation) {
        this.myEducation = myEducation;
    }

    public String getMySummary() {
        return mySummary;
    }

    public void setMySummary(String mySummary) {
        this.mySummary = mySummary;
    }
}
